+++
date = "2019-10-24"
weight = 100

title = "Component Repository Layout"

aliases = [
    "/old-wiki/Guidelines/Git-based_packaging_workflow",
    "/old-wiki/Guidelines/Gitlab-based_packaging_workflow"
]
+++

Packaging in Apertis is handled through a set of git repositories hosted on
GitLab containing the sources for every package. GitLab CI pipelines take care
of propagating updates to OBS to build and publish the resulting binaries.

{{% notice info %}}
For an overview of the broader Apertis workflow see
[The Apertis Workflow]({{< ref "workflow-guide.md" >}}). More detail on the
steps required to add and maintain packages can be found in
[Adding and Updating Components]({{< ref "component_guide.md" >}}).
{{% /notice %}}

# Repository contents

The packaging git repositories follow the
[DEP-14](http://dep.debian.net/deps/dep14/) Git layout specification.

![DEP-14 in Apertis](/images/apertis-dep-14-gitlab-curves.svg)

The repositories contain the following branches:
- `upstream/${UPSTREAM_RELEASE}`: Branches with the upstream project code (e.g.
  `upstream/buster`). Equivalent to the content obtained by unpacking an
  upstream release archive.

- `debian/${UPSTREAM_RELEASE}`: Branches with the Debian packaging information
  and changes laid on top of the upstream project code (e.g. `debian/buster`).

- `apertis/${APERTIS_RELEASE}`: Branches where the Debian branches above get
  merged with any modifications made for the Apertis packaging
  (e.g. `apertis/v2019`).

- `apertis/${APERTIS_RELEASE}-security` and `apertis/${APERTIS_RELEASE}-updates`
  (for instance, `apertis/v2019-updates`): similar to `apertis/${APERTIS_RELEASE}`
  but respectively target the Security and Updates repositories for published
  stable releases as described in [Process after a product
  release]( {{< ref "release-flow.md#process-after-a-product-release" >}} )

- `pristine-lfs`: Branch containing the references required fetch the original
  tarballs stored in [Git-LFS](https://git-lfs.github.com/).
  {{% notice warning %}}
  This branch should only ever be touched via the 
  [pristine-lfs](https://salsa.debian.org/pristine-lfs-team/pristine-lfs)
  utility.
  {{% /notice %}}
- `pristine-lfs-source`: stores references to the Git-LFS-hosted packaging
  tarballs, mainly to ensure that each (package, version) tuple is built only
  once and no conflicts can arise

  {{% notice warning %}}
  This branch is used by the Apertis automation and should never be touched.
  {{% /notice %}}

{{% notice note %}}
The DEP-14 specification also describes how tags are to be handled. These are
used by Apertis, but they are handled automatically by the automation, manually
setting such tags is strongly discouraged. These tags are:

- `debian/*`: tags for Debian releases in the `debian/*` branches
- `apertis/*`:  tags for the Apertis releases in the `apertis/*` branches
{{% /notice %}}

# Handling Git Attributes

[Git Attributes](https://git-scm.com/docs/gitattributes) is a feature that
enables modifications to be automatically made by git when certain operations
are carried out (such as `checkout`, `check-in` and `clone`) to enforce certain
project standards. However, this has the potential effect of modifying files and
that's not what we want during the packaging process.

Many upstream projects ship a `.gitattributes` file in their source
repositories, which perform operations that are tightly tied to the projects
development workflows. Certain settings in `.gitattributes` can lead to
problems in our packaging workflow, such as modifying the tree prior to checks
that no local modifications have been performed.

The following command can be run, which will override any such inherited
settings from the `.gitattributes` file:

    echo "* -text -eol -crlf -ident -filter -working-tree-encoding -export-subst" > .git/info/attributes

This will override the settings for the mentioned features for all files in the
repository.
