+++
date = "2018-11-19"
weight = 100

title = "LQA"

aliases = [
    "/old-wiki/QA/LQA"
]
+++

LQA is both a tool and API for LAVA quality assurance tasks. It stands
for **LAVA Quality Assurance** and its features go from submitting LAVA
jobs, collect tests results, and query most of the LAVA metadata.

It is a tool developed by Collabora and it is the standard way to submit
test jobs for Apertis images.

### Installation

Fetch the latest code from the git repository and install using python
**pip** as it will handle automatically the required dependencies::

    $ git clone https://gitlab.collabora.com/collabora/lqa
    $ pip install --user lqa/

An alternative method is to use Python virtual environment to confine
LQA dependencies to a specific directory instead of system wide.

    git clone https://gitlab.collabora.com/collabora/lqa
    python3 -m venv venv
    source venv/bin/activate
    pip install pyyaml
    pip install lqa/
    deactivate

Now you can make a symlink to the lqa script inside the venv/bin
directory for running it by simply calling **lqa**.

    cd ~/bin
    ln -s /<path to where the venv dir is>/venv/bin/lqa lqa

### Configuration

The lqa tool uses a global configuration file with the default name
`lqa.yaml`. This file can be found by lqa in different ways, though the
simplest way is to place this file inside the directory `~/.config/`.

The configuration file has three main fields: `user`, `auth-token` and
`server`.

These names are very self-explanatory, `user` is your LAVA user name,
`auth-token` is the LAVA API token (generated from the web UI at
<https://lava.collabora.co.uk/api/tokens/>), and `server` is the LAVA
server URL address:

    user: '<REPLACE_THIS_WITH_YOUR_LAVA_USERNAME>'
    auth-token: '<REPLACE_THIS_WITH_YOUR_API_TOKEN>'
    server: 'https://lava.collabora.co.uk/'

Save this file as `~/.config/lqa.yaml` and now test your configuration:

    $ lqa whoami
    <LAVA_USERNAME>

If everything is good, the above command should show your authenticated
user name and you should be all set to start using lqa\!
